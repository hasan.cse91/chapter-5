import java.util.Scanner;

public class Exercise10 {
    public static void main(String[] args){
        int height;
        int age;
        Scanner scanner = new Scanner(System.in);

        System.out.println("Enter Height");
        height = scanner.nextInt();
        System.out.println("Enter Age");
        age = scanner.nextInt();

        if(height>=140 && height<=230){
            double recommendedWeight = (height - 100 + age / 10) * 0.90;
            System.out.println("Recommended wight is: " + recommendedWeight);
        } else
            System.out.println("Height is invalid");
    }
}
