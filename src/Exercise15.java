public class Exercise15 {

    public static void main(String[] args){
        Triangle triangle = new Triangle(5,6,7);

        System.out.println(triangle.getArea());
        System.out.println(triangle.getPerimeter());
    }

    private static class Triangle{

        private final String INVALID_DIMENSION = "Invalid Dimension";
        int a;
        int b;
        int c;

        public Triangle(int a, int b, int c) {
            this.a = a;
            this.b = b;
            this.c = c;
        }

        public String getArea(){
            if(isValid()) {
                int s = a + b + c;
                return "Area is: " + String.valueOf(Math.sqrt(s*(s-a)*(s-b)*(s-c)));
            }
            else
                return INVALID_DIMENSION;
        }

        public String getPerimeter(){
            if(isValid())
                return "Perimeter is: " + String.valueOf(a+b+c);
            else
                return INVALID_DIMENSION;
        }

        private boolean isValid(){
            return a+b>c && b+c>a && c+a>b;
        }
    }
}
