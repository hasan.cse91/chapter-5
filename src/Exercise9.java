import java.util.Scanner;

public class Exercise9 {

    public static void main(String[] args){
        int power;
        Scanner scanner = new Scanner(System.in);

        power = scanner.nextInt();

        switch (power){
            case 6:
                printMessage("Million");
                break;
            case 9:
                printMessage("Billion");
                break;
            case 12:
                printMessage("Trillion");
                break;
            case 15:
                printMessage("Quadrillion");
                break;
            case 18:
                printMessage("Quintillion");
                break;
            case 21:
                printMessage("Sextillion");
                break;
            case 30:
                printMessage("Nonillion");
                break;
            case 100:
                printMessage("Googol");
                break;

            default:
                printMessage("Your input is out of range");

        }
    }

    private static void printMessage(String message){
        System.out.println(message);
    }
}
