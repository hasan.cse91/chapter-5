import java.util.Scanner;

public class Exercise11 {

    public static void main(String[] args){
        int height;
        double weight;
        int age;
        Scanner scanner = new Scanner(System.in);

        System.out.println("Enter Height");
        height = scanner.nextInt();
        System.out.println("Enter Wight");
        weight = scanner.nextDouble();
        System.out.println("Enter Age");
        age = scanner.nextInt();

        if(height>=140 && height<=230){
            double recommendedWeight = (height - 100 + age / 10) * 0.90;

            if((weight - recommendedWeight) > 20)
                System.out.println("You need more nourishment");
            else if ((weight - recommendedWeight) > 10)
                System.out.println("Exercise More");
            else
                System.out.println("Your recommended weight is: " + recommendedWeight);

        } else
            System.out.println("Height is invalid");
    }

}
